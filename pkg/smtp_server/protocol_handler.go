package smtp_server

import (
	"errors"
	"fmt"
	"strings"
)
import log "github.com/sirupsen/logrus"

type SMTPCode int
type State int

type ReplyList []Reply

//Callbacks
type HELOCallback func(hostname string) ReplyList
type EHLOCallback func(hostname string) ReplyList
type QUITCallback func() ReplyList

const (
	SMTPReady                       = SMTPCode(220) //<domain> Service ready
	SMTPClosingChannel              = SMTPCode(221) //<domain> Service closing transmission channel
	SMTPOk                          = SMTPCode(250) //Requested mail action okay, completed
	SMTPForwarded                   = SMTPCode(251) //User not local; will forward to <forward-path>
	SMTPNoVRFY                      = SMTPCode(252) //Cannot VRFY user, but will accept message and attempt delivery
	SMTPStartMail                   = SMTPCode(354) //Start mail input; end with <CRLF>.<CRLF>
	SMTPNotAvailable                = SMTPCode(421) //<domain> Service not available, closing transmission channel
	SMTPMailboxUnavailable          = SMTPCode(450) //Requested mail action not taken: mailbox unavailable (e.g., mailbox busy or temporarily blocked for policy reasons)
	SMTPAborted                     = SMTPCode(451) //Requested action aborted: local error in processing
	SMTPStorageFull                 = SMTPCode(452) //Requested action not taken: insufficient system storage
	SMTPParameterProblem            = SMTPCode(455) //Server unable to accommodate parameters
	SMTPSyntaxError                 = SMTPCode(500) //Syntax error, command unrecognized
	SMTPParameterError              = SMTPCode(501) //Syntax error in parameters or arguments
	SMTPNotImplemented              = SMTPCode(502) //Command not implemented
	SMTPBadSequence                 = SMTPCode(503) //Bad sequence of commands
	SMTPMailboxUnavailablePermanent = SMTPCode(550) //Requested action not taken: mailbox unavailable (e.g., mailbox not found, no access, or command rejected for policy reasons)
	SMTPNotLocalPermanent           = SMTPCode(551) //User not local; please try <forward-path>
)

const (
	StateConnected = State(iota)
	StateIdentified
	StateQuit
)

type Message struct {
	Command string
	Args    string
	Orig    string
}

func ParseMessage(msg string) (Message, error) {
	message := Message{}
	message.Orig = msg
	split := strings.SplitN(msg, " ", 2)
	if len(split) == 0 {
		return message, errors.New("Malformed.")
	}
	message.Command = strings.ToUpper(split[0])
	if len(split) > 1 {
		message.Args = split[1]
	}

	return message, nil
}

type Reply struct {
	Code int
	Text string
}

func ReplyToString(replies ReplyList) string {
	l := len(replies)
	if l == 0 {
		return ""
	}
	if l == 1 {
		return fmt.Sprintf("%d %s\r\n", replies[0].Code, replies[0].Text)
	}
	var ret strings.Builder
	for _, reply := range replies[:l-1] {
		fmt.Fprintf(&ret, "%d-%s\r\n", reply.Code, reply.Text)
	}
	fmt.Fprintf(&ret, "%d %s\r\n", replies[l-1].Code, replies[l-1].Text)
	return ret.String()
}

type SMTPProtocolHandler struct {
	state         State
	rawMode       bool
	heloCallbacks []HELOCallback
	ehloCallbacks []EHLOCallback
	quitCallbacks []QUITCallback
}

func NewSMTPProtocolHandler() *SMTPProtocolHandler {
	handler := SMTPProtocolHandler{state: StateConnected}
	return &handler
}

func (h *SMTPProtocolHandler) AddHELOCallback(callback HELOCallback) {
	h.heloCallbacks = append(h.heloCallbacks, callback)
}

func (h *SMTPProtocolHandler) AddEHLOCallback(callback EHLOCallback) {
	h.ehloCallbacks = append(h.ehloCallbacks, callback)
}

func (h *SMTPProtocolHandler) AddQUITCallback(callback QUITCallback) {
	h.quitCallbacks = append(h.quitCallbacks, callback)
}

func (h *SMTPProtocolHandler) HandleMessage(msg Message) (ReplyList, error) {
	log.Debugln(msg.Orig)
	var replies ReplyList

	switch msg.Command {
	case "EHLO":
		replies = h.EHLO(msg.Args)
		break
	case "HELO":
		replies = h.HELO(msg.Args)
		break
	case "QUIT":
		replies = h.QUIT()
		break
	}

	return replies, nil
}

func (h *SMTPProtocolHandler) EHLO(args string) ReplyList {
	var replies ReplyList
	for _, callback := range h.ehloCallbacks {
		r := callback(args)
		if len(r) > 0 {
			replies = append(replies, r...)
		}
	}

	h.state = StateIdentified
	return replies
}

func (h *SMTPProtocolHandler) HELO(args string) ReplyList {
	var replies ReplyList
	for _, callback := range h.heloCallbacks {
		r := callback(args)
		if len(r) > 0 {
			replies = append(replies, r...)
		}
	}

	h.state = StateIdentified
	return replies
}

func (h SMTPProtocolHandler) MAIL(args string) {

}

func (h SMTPProtocolHandler) RCPT(args string) {

}

func (h SMTPProtocolHandler) DATA(args string) {

}

func (h SMTPProtocolHandler) RSET(args string) {

}

func (h SMTPProtocolHandler) NOOP(args string) {

}

func (h *SMTPProtocolHandler) QUIT() ReplyList {
	var replies ReplyList
	for _, callback := range h.quitCallbacks {
		r := callback()
		if len(r) > 0 {
			replies = append(replies, r...)
		}
	}

	h.state = StateQuit
	return replies
}

func (h SMTPProtocolHandler) VRFY(args string) ReplyList {
	return ReplyList{}
}

func (h *SMTPProtocolHandler) GetState() State {
	return h.state
}
