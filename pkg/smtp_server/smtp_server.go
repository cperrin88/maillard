package smtp_server

import (
	"github.com/prometheus/common/log"
	"io"
	"net"
	"net/textproto"
)

type SMTPServer struct {
	listener net.Listener
	handler *SMTPProtocolHandler
	shutDown bool

}

func NewSMTPServer() SMTPServer  {
	server := SMTPServer{}
	server.handler = NewSMTPProtocolHandler()
	server.handler.AddHELOCallback(server.HELOCallback)
	server.handler.AddEHLOCallback(server.EHLOCallback)
	server.handler.AddQUITCallback(server.QUITCallback)
	return server
}

func (s SMTPServer) Serve(address string) error {
	var err error

	s.listener, err = net.Listen("tcp", address)
	if err!= nil {
		return err
	}

	defer s.listener.Close()

	for {
		conn, err := s.listener.Accept()
		if err != nil {
			return err
		}
		go s.HandleConnection(conn)
	}
}


func (s *SMTPServer) HandleConnection(conn net.Conn) error {
	text := textproto.NewConn(conn)

	defer text.Close()

	for {
		line, err := text.ReadLine()
		if err != nil {
			if err != io.EOF {
				return err
			}
			break
		}
		msg,_ := ParseMessage(line)

		rep,_ := s.handler.HandleMessage(msg)
		send := ReplyToString(rep)
		log.Debug(send)
		conn.Write([]byte(send))
		state := s.handler.GetState()
		if state == StateQuit {
			break
		}
	}
	return nil
}


func (s SMTPServer) EHLOCallback(hostname string) ReplyList {
	return []Reply{
		{250, "<servername>"},
		{250, "DSN"},
	}
}

func (s SMTPServer) HELOCallback(hostname string) ReplyList {
	return []Reply{
		{250, "<servername>"},
	}
}

func (s *SMTPServer) QUITCallback() ReplyList {
	return []Reply{
		{221, "Bye"},
	}
}