package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"maillard/pkg/smtp_server"
)

func main() {
	ss := smtp_server.NewSMTPServer()
	log.SetLevel(log.DebugLevel)
	fmt.Print(ss.Serve(":12345"))
}
